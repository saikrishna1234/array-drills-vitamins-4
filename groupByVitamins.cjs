let items = require("./3-arrays-vitamins.cjs");

let vitaminsByGroup = {};

items.map((eachItem) => {
  let vitamins = eachItem.contains.split(",");
  vitamins.map((eachVitamin) => {
    let vitamin = eachVitamin.trim();
    if (vitaminsByGroup[vitamin]) {
      vitaminsByGroup[vitamin].push(eachItem.name);
    } else vitaminsByGroup[vitamin] = [];
    vitaminsByGroup[vitamin].push(eachItem.name);
  });
});

console.log(vitaminsByGroup);

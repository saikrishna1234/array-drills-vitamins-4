let items = require("./3-arrays-vitamins.cjs");

function sorted(a, b) {
  if (a.contains.split(",").length > b.contains.split(",").length) return -1;
  else if (a.contains.split(",").length < b.contains.split(",").length)
    return 1;
}

console.log(items.sort(sorted));
